import { Component, OnInit } from '@angular/core';
import {FormGroup , FormControl, Validators} from '@angular/forms' ;
@Component({
  selector: 'app-form-details',
  templateUrl: './form-details.component.html',
  styleUrls: ['./form-details.component.css']
})
export class FormDetailsComponent  {
 
  formDetails  = new FormGroup(
    {
      firstName: new FormControl('' ),
      lastname: new FormControl(''),
      mobileno :new FormControl(''),
      email: new FormControl(''),
    addresss: new FormControl(''),
   
       city:new FormControl(''),
       state: new FormControl(''),
       pincode: new FormControl('')
        
    }
  );

onSubmit() {
  console.warn(this.formDetails.value);

}
}